import React, { Component } from 'react';
import Swiper from 'react-id-swiper';

import MainSliderSingle from '../MainSliderSingle/MainSliderSingle';
import MainSliderSingleVideo from "../MainSliderSingleVideo/MainSliderSingleVideo";

class MainSlider extends Component {

  render() {
    const params = {
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
      },

      lazy: true,
      effect: 'fade',
      autoplay: {
        delay: 8000,
        disableOnInteraction: false
      }
    };

    return (
      <section className="main-slider">
        <Swiper {...params} >
          <div>
            <MainSliderSingleVideo
              header="main-slider__single--shipwreck-tour"
              subHeading="See beautiful"
              mainHeading="Shipwreck Beach"
              paragraph="Find you perfect vacation"
              linkTo="/tours/1"
            />
          </div>
          {/*<div>*/}
          {/*  <MainSliderSingle*/}
          {/*    header="main-slider__single--shipwreck-tour"*/}
          {/*    subHeading="See beautiful"*/}
          {/*    mainHeading="Shipwreck Beach"*/}
          {/*    paragraph="Find you perfect vacation"*/}
          {/*    linkTo="/tours/1"*/}
          {/*  />*/}
          {/*</div>*/}
          <div>
            <MainSliderSingle
              header="main-slider__single--olympia-tour"
              subHeading="Birthplace of the olympic games"
              mainHeading="Olympia Tour"
              paragraph="Discover it with us!"
              linkTo="/tours/4"
            />
          </div>
          <div>
            <MainSliderSingle
              header="main-slider__single--turtle-tour"
              subHeading="Open your eyes to"
              mainHeading="The Hidden World"
              paragraph="Turtle Cruise and Keri Sunset Tour"
              linkTo="/tours/2"
            />
          </div>

        </Swiper>
      </section>
    )
  }
}

export default MainSlider;
