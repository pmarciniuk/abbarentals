import React from 'react';

function Toolbar(props) {
  return (
    <div className="toolbar">
      {props.children}
    </div>
  )
}

export default Toolbar;