import React from 'react';
import Button from '../Button/Button';

function MainSliderSingle(props) {
  return (
    <div className={'main-slider__single ' + (props.header ? props.header : '')}
    >
      <div className="main-slider__content">
      
        <h2 className="main-slider__text-heading 
                        u-margin-bottom-small"
        >
          <span className="main-slider__text-subheading">
            {props.subHeading}
          </span>
          <span className="main-slider__text-main-heading">
            {props.mainHeading}
          </span>
        </h2>
        <p className="main-slider__text-paragraph
                      u-margin-bottom-medium"
        >
          {props.paragraph}
        </p>
        <Button
          classes="button--primary"
          link={props.linkTo}
        >
          Learn more
        </Button>
      </div>
    </div>
    

  )
}

export default MainSliderSingle;